// Rumfangsberenger
// Eksempel at regne et rumfang.
// Dato: 05-09-2016
// Version: 1.00


package com.company;

import java.util.Scanner;

public class Main {

    // Programmet starter her
    public static void main(String[] args) {


        double radius = 0, højde = 0, rumfang;
        boolean isOk = false;

        Scanner input = new Scanner(System.in);

        System.out.println("Indtast radius");

        while (!isOk) {
            try {
                radius = input.nextDouble();
                isOk = true;
            } catch (Exception e) {

                //Når inputtet er ulovligt fjernes den ulovlige indtastning.
                input.nextLine();

                //Brugeren bliver gjort opmærksom på han laver noget forkert.
                System.out.println("Indtastning forkert, prøv igen.");
            }
        }

        System.out.println("Indtast højde");
        isOk = false;

        while (!isOk) {

            try {
                højde = input.nextDouble();
                isOk = true;
            } catch (Exception e) {
                //e.printStackTrace();
                input.nextLine();
                System.out.println("Indtastning forkert, prøv igen.");

            }
        }


        rumfang = højde * Math.PI * (radius * radius);


        if (rumfang >= 0) {
            System.out.println("Rumfanget er lig med " + rumfang);
        } else {
            System.out.println("Rumfanget er lig med " + (-rumfang));

        }


    }
}
